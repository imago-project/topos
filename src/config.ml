external env : string = "import.meta.env.MODE" [@@bs.val]

external base_url : string = "import.meta.env.BASE_URL" [@@bs.val]

external is_prod : bool = "import.meta.env.PROD" [@@bs.val]

external is_dev : bool = "import.meta.env.DEV" [@@bs.val]

external matrix_url : string = "import.meta.env.VITE_MATRIX_URL" [@@bs.val]

external matrix_homeserver : string = "import.meta.env.VITE_MATRIX_HOMESERVER"
  [@@bs.val]

external api_url : string = "import.meta.env.VITE_API_URL" [@@bs.val]
