(* [%raw {|require.context('../../../node_modules/bytesize-icons/dist/icons', false, /\.svg$/)|}] *)

external register_icons : string = "virtual:svg-icons-register" [@@bs.module]

let ri = register_icons

let aria_label label = Vdom.attribute "" "aria-label" label

let icon ?(path = "") ?(prefix = "") ?(class' = "") name =
  let href = path ^ "#" ^ prefix ^ "icon-" ^ name in
  let open Vdom in
  fullnode "http://www.w3.org/2000/svg" "svg" "" ""
    [ attribute "" "class" (class' ^ " icon h-4 w-4 mr-0")
    ; attribute "" "aria-hidden" "true"
    ; attribute "" "focusable" "false" ]
    [ fullnode "http://www.w3.org/2000/svg" "use" "" ""
        [attribute "" "href" href]
        [] ]
