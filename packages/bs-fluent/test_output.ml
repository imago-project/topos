let simple_message lc =
{js|Simple message|js}

let _simple_term lc =
{js|Simple term|js}

type message_with_variable_params = { foo : string }
let message_with_variable (params : message_with_variable_params) lc =
{js|Message: Variable is |js} ^ params.foo

type _term_with_variable_params = { foo : string }
let _term_with_variable (params : _term_with_variable_params) lc =
{js|Term: Variable is |js} ^ params.foo

type message_with_simple_number_variable_params = { foo : int }
let message_with_simple_number_variable (params : message_with_simple_number_variable_params) lc =
{js|Message: Variable is |js} ^ (Fluent.number_format params.foo (Fluent.Runtime.make_number_params ()) lc)

type _term_with_simple_number_variable_params = { foo : int }
let _term_with_simple_number_variable (params : _term_with_simple_number_variable_params) lc =
{js|Term: Variable is |js} ^ (Fluent.number_format params.foo (Fluent.Runtime.make_number_params ()) lc)

type message_with_number_variable_params = { foo : int }
let message_with_number_variable (params : message_with_number_variable_params) lc =
{js|Message: Variable is |js} ^ (Fluent.number_format params.foo (Fluent.Runtime.make_number_params ~minimumIntegerDigits:3()) lc)

type _term_with_number_variable_params = { foo : int }
let _term_with_number_variable (params : _term_with_number_variable_params) lc =
{js|Term: Variable is |js} ^ (Fluent.number_format params.foo (Fluent.Runtime.make_number_params ~minimumIntegerDigits:3()) lc)

type message_with_datetime_variable_params = { foo : int }
let message_with_datetime_variable (params : message_with_datetime_variable_params) lc =
{js|Message: Variable is |js} ^ (Fluent.datetime_format params.foo (Fluent.Runtime.make_datetime_params ~weekday:"long"()) lc)

type _term_with_datetime_variable_params = { foo : int }
let _term_with_datetime_variable (params : _term_with_datetime_variable_params) lc =
{js|Term: Variable is |js} ^ (Fluent.datetime_format params.foo (Fluent.Runtime.make_datetime_params ~weekday:"long"()) lc)

let message_with_an lc =
{js|Message: Something|js}

let message_with_an_attribute lc =
{js|Message: Attribute|js}

type message_with_an_attribute_with_variable_params = { foo : string }
let message_with_an_attribute_with_variable (params : message_with_an_attribute_with_variable_params) lc =
{js|Message: Attribute: Variable is |js} ^ params.foo

let _term_with_an lc =
{js|Term: Something|js}

let _term_with_an_attribute lc =
{js|Term: Attribute|js}

let _term_with_an_simple_attribute lc =
{js|simple_attribute|js}

type _term_with_an_attribute_with_variable_params = { foo : string }
let _term_with_an_attribute_with_variable (params : _term_with_an_attribute_with_variable_params) lc =
{js|Term: Attribute: Variable is |js} ^ params.foo

type _term_with_an_simple_attribute_with_variable_params = { foo : string }
let _term_with_an_simple_attribute_with_variable (params : _term_with_an_simple_attribute_with_variable_params) lc =
{js|simple_attribute_with_|js} ^ params.foo

let message_with_message_reference lc =
{js|Message: Message is |js} ^ (simple_message lc)

let _term_with_message_reference lc =
{js|Term: Message is |js} ^ (simple_message lc)

let message_with_term_reference lc =
{js|Message: Term is |js} ^ (_simple_term lc)

let _term_with_term_reference lc =
{js|Term: Term is |js} ^ (_simple_term lc)

let message_with_message_attribute_reference lc =
{js|Message: Message attribute is |js} ^ (message_with_an_attribute lc)

let _term_with_message_attribute_reference lc =
{js|Term: Message attribute is |js} ^ (message_with_an_attribute lc)

let message_with_parameterized_term_reference lc =
{js|Message: Term is |js} ^ (_term_with_variable { foo = "bar" } lc)

let _term_with_parameterized_term_reference lc =
{js|Term: Term is |js} ^ (_term_with_variable { foo = "bar" } lc)

let message_with_number_parameterized_term_reference lc =
{js|Message: Term is |js} ^ (_term_with_number_variable { foo = 6 } lc)

let _term_with_number_parameterized_term_reference lc =
{js|Term: Term is |js} ^ (_term_with_number_variable { foo = 6 } lc)

let message_with_special_char lc =
{js|Message: opening curly brace: |js} ^ "{" ^ {js|.|js}

let _term_with_special_char lc =
{js|Term: opening curly brace: |js} ^ "{" ^ {js|.|js}

type message_with_selector_expecting_text_params = { case : string }
let message_with_selector_expecting_text (params : message_with_selector_expecting_text_params) lc =
(match params.case with
| "locative" ->
{js|Locative|js}
| _ ->
{js|Nominative|js})


type message_with_selector_expecting_number_as_ordinal_params = { pos : int }
let message_with_selector_expecting_number_as_ordinal (params : message_with_selector_expecting_number_as_ordinal_params) lc =
(match ((Fluent.number_format params.pos (Fluent.Runtime.make_number_params ()) lc), Fluent.plural_rule params.pos lc ~_type:"ordinal") with
| "1", _ | _, "1" ->
{js|first!|js}
| "one", _ | _, "one" ->
(Fluent.number_format params.pos (Fluent.Runtime.make_number_params ()) lc) ^ {js|st|js}
| "two", _ | _, "two" ->
(Fluent.number_format params.pos (Fluent.Runtime.make_number_params ()) lc) ^ {js|nd|js}
| "few", _ | _, "few" ->
(Fluent.number_format params.pos (Fluent.Runtime.make_number_params ()) lc) ^ {js|rd|js}
| _, _ ->
(Fluent.number_format params.pos (Fluent.Runtime.make_number_params ()) lc) ^ {js|th|js})


let message_with_selector_expecting_term_attribute lc =
(match (_term_with_an_simple_attribute lc) with
| "simple_attribute" ->
{js|Match|js}
| _ ->
{js|No match|js})


let message_with_selector_expecting_term_attribute_with_variable lc =
(match (_term_with_an_attribute_with_variable { foo = "bar" } lc) with
| "simple_attribute_with_bar" ->
{js|Match|js}
| _ ->
{js|No match|js})


type message_with_selector_having_references_in_variants_params = { case : string ; foo : string }
let message_with_selector_having_references_in_variants (params : message_with_selector_having_references_in_variants_params) lc =
(match params.case with
| "locative" ->
{js|Locative: Variable is |js} ^ params.foo ^ {js|, term is $|js} ^ (_simple_term lc)
| "other" ->
{js|Other: term with variable is $|js} ^ (_term_with_variable { foo = "bar" } lc)
| _ ->
{js|Nominative: Variable is |js} ^ params.foo ^ {js|, message is $|js} ^ (simple_message lc))


type message_with_nested_selectors_params = { case : string ; count : string }
let message_with_nested_selectors (params : message_with_nested_selectors_params) lc =
(match params.case with
| "locative" ->
{js|Locative|js}
| _ ->
{js|Nominative |js} ^ (match params.count with
| "one" ->
{js|-> One|js}
| _ ->
{js|-> Other|js})
)
