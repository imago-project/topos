import { FluentBundle } from "fluent-runtime/bundle";
import { Runtime } from "fluent-runtime";
const { isol, select, NUMBER } = Runtime(["en"]);
const R = new Map([

// ## Closing tabs

["tabs-close-button", { value: $ => ["Close"] }],
["tabs-close-tooltip", { value: $ => [select($.tabCount, "other", { one: ["Close ", isol(NUMBER({ ...$, type: "ordinal" }, $.tabCount)), " tab"], other: ["Close ", isol($.tabCount), " tabs"] })] }],
["tabs-close-warning", {
  value: $ => ["You are about to close ", isol($.tabCount), " tabs.\nAre you sure you want to continue?"],
  attributes: { "bidule": $ => ["Truc"] }
}],

// ## Syncing

["-sync-brand-name", { value: $ => ["Firefox Account"] }],
["sync-dialog-title", { value: $ => [R.get("-sync-brand-name").value()] }],
["sync-headline-title", { value: $ => [isol(R.get("-sync-brand-name").value()), ": The best way to bring\nyour data always with you"] }],
["sync-signedout-title", { value: $ => ["Connect with your ", isol(R.get("-sync-brand-name").value())] }],

]);
export default new FluentBundle(["en"], R);
