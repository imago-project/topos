(* t.ml *)

external runtime : string -> string = "Runtime" [@@bs.module "fluent-runtime"]

let runtimes = Js.Dict.fromList [("en", runtime("en"))]

let runtime_for_locale lc = Js.Dict.get runtimes lc

type tabs_close_tooltip_params = {tabCount : int }

let tabs_close_tooltip locale params =
  match locale with
  | "en" ->
    [ select params.tabCount "other"
        { one = ["Close ", isol params.tabCount, "tab"]
        ; other = ["Close ", isol params.tabCount, "tabs"]
        }
    ]
    |> Bundle.msgString [locale]
  | _ -> "thing"
