// const { property } = require('safe-identifier')

function property(namespace, name) {
  if (namespace === null) {
    return name.replace(/-/g, '_')
  }
  else {
    return `${namespace.replace(/-/g, '_')}_${name.replace(/-/g, '_')}`
  }
}

module.exports = class FluentCompiler {
  constructor({
    runtime = 'bundle',
    runtimeGlobals = ['DATETIME', 'NUMBER'],
    runtimePath = 'fluent-runtime',
    useIsolating = true,
    withJunk = false
  } = {}) {
    this.runtime = runtime
    this.runtimeGlobals = runtimeGlobals
    this.runtimePath = runtimePath
    this.useIsolating = useIsolating
    this.withJunk = withJunk
    this._rtImports = {}
  }

  compile(locales, resource) {
    if (resource.type !== 'Resource') {
      throw new Error(`Unknown resource type: ${resource.type}`)
    }

    const lc = locales
      ? JSON.stringify(Array.isArray(locales) ? locales : [locales])
      : 'undefined'
    this._rtImports = { isol: false, select: false }
    for (const fn of this.runtimeGlobals) this._rtImports[fn] = false

    const head = []
    const body = resource.body
      .filter(entry => entry.type !== 'Junk' || this.withJunk)
      .map(entry => this.entry(entry)) // may modify this._rtImports
    const foot = []

    // const rt = Object.keys(this._rtImports).filter(key => this._rtImports[key])
    // if (rt.length > 0) {
    //   head.unshift(
    //     `import { Runtime } from "${this.runtimePath}";`,
    //     `const { ${rt.join(', ')} } = Runtime(${lc});`
    //   )
    // }

    // switch (this.runtime) {
    //   case 'bundle':
    //     head.unshift(
    //       `import { FluentBundle } from "${this.runtimePath}/bundle";`
    //     )
    //     foot.push(`export default new FluentBundle(${lc}, R);`)
    //     break
    //   case 'resource':
    //     foot.push('export default R;')
    //     if (locales) foot.push(`export const locales = ${lc};`)
    //     break
    //   default:
    //     throw new Error(`Unknown runtime ${JSON.stringify(this.runtime)}`)
    // }
    return `${head.join('\n')}\n\n${body.join('\n').trim()}\n\n${foot.join(
      '\n'
    )}\n`
  }

  entry(entry) {
    switch (entry.type) {
      case 'Message':
        return this.message(entry)
      case 'Term':
        return this.term(entry)
      case 'Comment':
        return this.comment(entry, '')
      case 'GroupComment':
        return this.comment(entry, '##')
      case 'ResourceComment':
        return this.comment(entry, '###')
      case 'Junk':
        return this.junk(entry)
      default:
        throw new Error(`Unknown entry type: ${entry.type}`)
    }
  }

  comment(comment, prefix = '//') {
    const cc = comment.content
      .split('\n')
      .map(line => (line.length ? `(* ${prefix} ${line} *)` : `(* ${prefix} *)`))
      .join('\n')
    return `\n${cc}\n`
  }

  junk(junk) {
    return junk.content.replace(/^/gm, '// ')
  }

  message(message) {
    const head = message.comment ? this.comment(message.comment) : ''
    const name = property(null, `${message.id.name}`)
    const references = this.variable_references(message.value)
    console.log(`message references:`)
    console.log(references)
    const value = message.value ? this.pattern(message.value, false, references) : ' null'
    const body = this.messageBody(name, value, message.attributes, references)
    return head + body
  }

  term(term) {
    const head = term.comment ? this.comment(term.comment) : ''
    const name = property(null, `_${term.id.name}`)
    const references = this.variable_references(term.value)
    console.log(`message references:`)
    console.log(references)
    const value = this.pattern(term.value, false, references)
    const body = this.messageBody(name, value, term.attributes, references)
    return head + body
  }

  messageBody(mname, value, attributes, references) {
    const attr = []
    mname = property(null, mname)
    for (const attribute of attributes) {
      const name = property(mname, attribute.id.name)
      const value = this.pattern(attribute.value, false, references)
      attr.push(`let ${name} params lc = ${value} |> Fluent.formatString`)
    }
    let params_type
    let fun_declaration
    const references_types = Object.entries(references).map(e => `${e[0]} : ${e[1]}`)
    if (references_types.length) {
      params_type = `type ${mname}_params = { ${references_types.join('; ')} }`
      fun_declaration = `let ${mname} (params : ${mname}_params) lc = ${value} |> Fluent.formatString`
    } else {
      params_type = ''
      fun_declaration = `let ${mname} (_params : unit) lc = ${value} |> Fluent.formatString`
    }
    return [
      params_type,
      fun_declaration,
      ...attr
    ].join('\n')
  }

  pattern(pattern, inVariant, references = {}) {
    const singleElement = pattern.elements.length === 1
    const useIsolating = this.useIsolating && (inVariant || !singleElement)
    const content = []
    for (const el of pattern.elements) {
      content.push(this.element(el, useIsolating, references))
    }
    // if (inVariant && singleElement) {
    //   return ` ${content[0]}`
    // }
    return `Fluent.List [${content.join('; ')}]`
  }

  element(element, useIsolating, references = {}) {
    switch (element.type) {
      case 'TextElement':
        return `Fluent.Literal (Fluent.String ${JSON.stringify(element.value)})`
      case 'Placeable': {
        const expr = this.expression(element.expression, references)
        // if (useIsolating) {
        //   this._rtImports.isol = true
        //   return `Isol ${expr}`
        // }
        return expr
      }
      default:
        throw new Error(`Unknown element type: ${element.type}`)
    }
  }

  reduce_references(expr_list) {
    return expr_list
      // .map(this.variable_references, this)
      .reduce(function (acc, currentObj) {
        Object.entries(currentObj).forEach((currentEntry) => {
          if (currentEntry[1] !== 'string' || !acc[currentEntry[0]])
            acc[currentEntry[0]] = currentEntry[1]
        })
        return acc
      }, {})
  }

  variable_references(expr, acc = {}) {
    switch (expr.type) {
      case 'Pattern': {
        return this.reduce_references(expr.elements.map(this.variable_references, this))
      }
      case 'VariableReference': {
        // return `${expr.id.name} : ${type}`
        if (!acc[expr.id.name])
          acc[expr.id.name] = 'string'
        return acc
      }
      case 'SelectExpression': {
        return this.reduce_references(
          [this.variable_references(expr.selector),
            ...expr.variants.map(this.variable_references, this)
          ])
      }
      case 'Placeable': {
        return this.variable_references(expr.expression)
      }
      case 'Variant': {
        return this.variable_references(expr.value)
      }
      case 'FunctionReference': {
        const fnName = expr.id.name
        switch (fnName) {
          case 'NUMBER':
            // return `${expr.arguments.positional[0].id.name} : int`
            acc[expr.arguments.positional[0].id.name] = 'int'
            return acc
          default:
            return this.variable_references(expr.arguments.positional[0])
          // case 'DATETIME'
        }
      }
      default:
        return acc
    }
  }

  expression(expr, references = {}) {
    switch (expr.type) {
      case 'StringLiteral':
        return `Fluent.Literal (Fluent.String "${expr.value}")`
      case 'NumberLiteral':
        return `Fluent.Literal (Fluent.Int ${expr.value})`
      case 'VariableReference':
        console.log('expression')
        console.log(references)
        console.log(expr.id.name)
        console.log(references[expr.id.name])
        switch (references[expr.id.name]) {
          case 'int':
            return `Fluent.Literal (Fluent.Int params.${expr.id.name})`
          default:
            return `Fluent.Literal (Fluent.String params.${expr.id.name})`
        }
      case 'TermReference': {
        let out
        if (expr.attribute)
          out = property(`_${expr.id.name}`, expr.attribute.name)
        else
          out = property(null, `_${expr.id.name}`)
        const args = this.termArguments(expr.arguments)
        return `Fluent.TermRef (${out}, ${args})`
      }
      case 'MessageReference': {
        let out
        if (expr.attribute)
          out = property(expr.id.name, expr.attribute.name)
        else
          out = property(null, expr.id.name)
        return `Fluent.MessageRef ${out}`
      }
      case 'FunctionReference': {
        const fnName = expr.id.name
        if (!this.runtimeGlobals.includes(fnName))
          throw new Error(`Unsupported global ${fnName}`)
        this._rtImports[fnName] = true
        const args = this.functionArguments(fnName, expr.arguments, references)
        return `Fluent.FunRef (${fnName} ${args})`
      }
      case 'SelectExpression': {
        const selector = this.expression(expr.selector, references)
        const defaultVariant = expr.variants.find(variant => variant.default)
        const defaultKey = `(${this.variantKey(defaultVariant.key)})`
        const variants = expr.variants.map(v => this.variant(v, references), this).join('; ')
        this._rtImports.select = true
        return `Fluent.Select ((${selector}), ${defaultKey}, [|${variants}|])`
      }
      case 'Placeable':
        return this.expression(expr.expression, references)
      default:
        throw new Error(`Unknown expression type: ${expr.type}`)
    }
  }

  termArguments(expr) {
    if (!expr || expr.named.length === 0) return '()'
    const named = expr.named.map(this.namedArgument, this)
    return `{ ${named.join('; ')} }`
  }

  functionArguments(fnName, expr, references = {}) {
    let ctx = '()'
    if (expr && expr.named.length > 0) {
      const named = expr.named.map(this.namedArgumentFunction, this)
      switch (fnName) {
        case 'NUMBER':
          ctx = `Fluent.make_number_params3 ${named.join(' ')} ()`
        // case 'DATETIME':
        //   ctx = `Fluent.make_datetime_params ${named.join(' ')}`
      }
    }
    if (expr && expr.positional.length > 0) {
      let positional
      switch (fnName) {
        case 'NUMBER':
          positional = `params.${expr.positional[0].id.name}`
          break
        default:
          positional = expr.positional.map(a => this.expression(a, references), this).join(' ')
      }
      return `(${ctx}, ${positional})`
    } else {
      return `${ctx}`
    }
  }

  namedArgument(arg) {
    let key = arg.name.name
    if (key === "type")
      key = "_type"
    let value
    switch (arg.value.type) {
      case 'StringLiteral':
        value = `"${arg.value.value}"`
      case 'NumberLiteral':
        value = `"${arg.value.value}"`
    }
    return `${key} = ${value}`
  }

  namedArgumentFunction(arg) {
    let key = arg.name.name
    if (key === "type")
      key = "_type"
    let value
    switch (arg.value.type) {
      case 'StringLiteral':
        value = `"${arg.value.value}"`
      case 'NumberLiteral':
        value = `${arg.value.value}`
    }
    return `~${key}:${value}`
  }

  variant(variant, references = {}) {
    const key = this.variantKey(variant.key)
    const value = this.pattern(variant.value, true, references)
    return `(${key}, ${value})`
  }

  variantKey(key) {
    switch (key.type) {
      case 'Identifier':
        return `Fluent.String "${key.name}"`
      case 'NumberLiteral':
        return `Fluent.Int ${key.value}`
      default:
        throw new Error(`Unknown variant key type: ${key.type}`)
    }
  }
}
