make ints be floats, improve DATETIME

get it working as a webpack-plugin (with watch)
write plugin.js in ocaml

refactor
write tests and docs (in mlis ?)
extract in its own repo

publish as npm package

handle comments
improve generated indentation, maybe run ocamlformat?
allow compiling in a file per locale
allow different runtime
generate mlis to make terms private
maybe have params like this: `let message {foo : string} lc = ...` (instead of defining a custom type for params) // rather ~foo:string...


```
learn-practice-test-note-html-div =
  {HTML("b", "PRACTICE")} test, scores are not counted.

learn-you-can-change-your-testing-method-html-div =
    You can change your testing method at any time in your {HTML($link, "preferences")}.
```

```
open Tea.Html

let learn_practice_test_note_html_div ?(attr=[]) () =
  div attr
    [ b [] [text "PRACTICE"]
    ; text " test, scores are not counted."
    ]

learn_you_can_change_your_testing_method_html_div ?(attr=[]) ~link () =
  div attr
    [ text "You can change your testing method at any time in your "
    ; link [text "preferences"]
    ]
```

```
let view =
  div []
    [ T.learn_practice_test_note_html_div ()
    ; T.learn_you_can_change_your_testing_method_html_div ~link:(a [href "https://example.com"])
    ]
```

https://gitlab.com/imago-project/imago_front/-/blob/3aacd4fcac69638e8a2655817702906b5c5e1e40/src/auth.ml#L30-43
(defining a function that I find missing in `bucklescript-tea`)

https://gitlab.com/imago-project/imago_front/-/blob/3aacd4fcac69638e8a2655817702906b5c5e1e40/src/locale.ml
(using a JS global variable if I don't want to pass the user locale in all my models, btw this should use `navigator.languages` to get the first locale use, and that might have its place in `bs-fluent`)

https://gitlab.com/imago-project/imago_front/-/blob/3aacd4fcac69638e8a2655817702906b5c5e1e40/src/chat.ml#L56
(bypassing the way an event object is defined in `bucklescript-tea`, because I know that a `keydown` event has a `ctrlKey` property)

https://gitlab.com/imago-project/imago_front/-/blob/3aacd4fcac69638e8a2655817702906b5c5e1e40/src/matrixclient/matrixclient.ml#L274-296

https://gitlab.com/imago-project/imago_front/-/blob/3aacd4fcac69638e8a2655817702906b5c5e1e40/src/matrix.ml#L49-55

https://gitlab.com/imago-project/imago_front/-/blob/master/src/sidebar.ml#L71

